(function () {
  document.getElementById("startButton").addEventListener("click", action);
  document.getElementById("shuffle").addEventListener("click", reset);

  var dices = document.getElementsByClassName("dices");
  var element = document.getElementsByClassName("element");

  var noOfElem = 0;
  var arg = [];
  var result = true;
  var choiceIndex = [];
  var trueValues = [];
  let markElements = [];
  let ramdomNumbers = [];

  function action() {
    createElements();
  }
  function reset(){
    for( let e = 0; e < 4; e++){
      document.getElementById("gameBox").firstChild.remove()
    }
    noOfElem = 0;
    arg = [];
    result = true;
    choiceIndex = [];
    trueValues = [];
    markElements = [];
    ramdomNumbers = [];
    createElements();
  }
  //the below function will create blocks of elements
  function createElements() {
    let gameBox = document.getElementById("gameBox")//locating the gamebox
    for( let noOfrows = 0; noOfrows < 4; noOfrows++){//creating 4 rows
      let row = document.createElement("div");
      row.className = "row";
      for( let noOfdices = 0; noOfdices < 4; noOfdices++) {// creating 4 elements for every row with a parent class named 'dices'
        let dice = document.createElement("div");
        dice.className = "dices";
        let elem = document.createElement("div");
        elem.className = "element";
        dice.appendChild(elem);
        row.appendChild(dice);
      }
      gameBox.appendChild(row);
    }
    generateRandomNumbers();
    assignValues(element);
  }
  //generate random numbers for every block
  function generateRandomNumbers() {
      let numbers = [];
      for(let i = 1; i <= 8; i++) {
          for(let j = 0; j < 2;j++) {
              numbers.push(i);
          }
      }
      function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }
      ramdomNumbers = shuffle(numbers);
  };
  //main function
  function assignValues(target) {
    for (; noOfElem < 16; noOfElem++) {
      (function (index) {
        content = target[index];
        var image = document.createElement("img"); 
        image.src = assignImageSrc(ramdomNumbers[index]);
        content.appendChild(image);
         // closure
        markElements[index] = function() {
          var choice = 0;
          choice = ramdomNumbers[index];
          choiceIndex.push(index);
          arg.push(choice);
          element[index].classList.add("show");
          if(element[index].classList.contains("show")) {
            dices[index].removeEventListener("click", markElements[index]);      
          }
          if(arg.length === 2) {
            setTimeout(perform, 200);
          }
        }
        dices[index].addEventListener("click", markElements[index]);
      })(noOfElem);
    }
  }

  function assignImageSrc(imageSrc) {
    var imagesource
    switch(imageSrc) {
      case 1: imagesource = "../../../assets/images/black-currant-iceceam-120x120.png";
      break;

      case 2: imagesource = "../../../assets/images/Butterscotch-Ice-Cream-500x375.jpg";
      break;
      case 3: imagesource = "../../../assets/images/chocolate-ice-cream-recipe-7-120x120.png";
      break;

      case 4: imagesource = "../../../assets/images/Homemade-Chocolate-Bars.jpg";
      break;

      case 5: imagesource = "../../../assets/images/marshmallow.jpg";
      break;
      
      case 6: imagesource = "../../../assets/images/national-candy.jpg";
      break;

      case 7: imagesource = "../../../assets/images/raspberry-ice-cream.jpg";
      break;

      case 8: imagesource = "../../../assets/images/vennila-icecream.jpg";
      break;
    }
     return imagesource;
  }


  function perform() {
    if (arg[0] === arg[1]) {
      arg.push(result);
      trueValues.push(choiceIndex[0], choiceIndex[1]);
    } else {
      result = false;
      arg.push(result);
    }
    show();
  }

  function show() {
    if (result === false) {
      for (var i = 0; i < choiceIndex.length; i++) {
        element[choiceIndex[i]].classList.remove("show");
      }
    }
    if (arg.length === 3) {
        arg = [];
        choiceIndex = [];
    }
    result = true;
    for(let noOfDices = 0; noOfDices < 16; noOfDices++) {
        if(trueValues[noOfDices] !== noOfDices) {
            dices[noOfDices].addEventListener("click", markElements[noOfDices]);
        }
    }
    for (var n = 0; n < trueValues.length; n++) {
        element[trueValues[n]].classList.add("show");
        dices[trueValues[n]].removeEventListener("click", markElements[trueValues[n]]);
    }
  }
})();