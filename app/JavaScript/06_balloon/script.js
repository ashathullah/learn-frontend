(function () {
  let balloons = [
    "../../../assets/images/meroonredBalloon.png",
    "../../../assets/images/voiletBalloon.png",
    "../../../assets/images/redBalloon.png",
    "../../../assets/images/skyblueBalloon.png",
    "../../../assets/images/peachblueBalloon.png",
  ];

  var state = true;
  var speedOfBalloons = [30, 20, 35, 35, 25];
  var randomBalloon = 0
  var unit = 0;
  let button = document.getElementsByClassName("IOBTN")[0];
  let createBalloonsID = 0;
  let moveID = 0;
  let score = 0;
  let myscore = 0;
  let totalBalloons = 0;
  let onscreenBalloons = 0;
  button.addEventListener("click", startStop);
  document.getElementsByClassName("reset")[0].addEventListener("click", reset);
  document.getElementsByClassName("IOBTN")[1].addEventListener("click", startStop);
  
  
  function startStop() {
      if(state === true) {
          button.innerHTML = "Pause";
          state = false;
          document.getElementsByClassName("scoreWindow")[0].classList.remove("show");
          action();
        }else{
          button.innerHTML = "Start";
          state = true;
          clearTimeout(createBalloonsID);
          clearTimeout(moveID);
          wipeOut();
          document.getElementsByClassName("scoreWindow")[0].classList.add("show");
        }
      }
      
      function reset() {
        score= 0;
        unit = 0;
        onscreenBalloons = 0;
        totalBalloons = 0;
        document.getElementsByClassName("scoreWindow")[0].classList.remove("show");
        document.getElementsByClassName("score")[0].innerHTML = "score :" + score +" / "+ unit;
        document.getElementsByClassName("score")[1].innerHTML = "score :" + myscore;
        wipeOut();
      }

  function wipeOut() {
    while(document.getElementById("balloons").childElementCount > 0) {
      let stop = document.getElementById("balloons").firstChild;
      stop.remove();
    }
  }

  function action() {
    createBalloonsID = setTimeout(createBalloons, 1000);
  }

  function createBalloons() {
    let balloon = document.getElementById("balloons");
    let bal = document.createElement("div");
    bal.id = "balloon" + unit;
    bal.style.display = "inline-block"
    bal.style.position = "absolute";
    bal.style.bottom = 0;
    bal.addEventListener("click", function destroy() {
      bal.remove();
      score++;
    });
    unit++;
    (function () {
      let image = document.createElement("img");
      randomBalloon = Math.floor(Math.random()*balloons.length);
      image.src = balloons[randomBalloon];
      bal.appendChild(image);
    })();
    balloon.appendChild(bal);
    fly(bal.id)
    action();
  };
  
  function fly(object) {
    var speed = 0;
    (function () {
      speed = speedOfBalloons[Math.floor(Math.random()*speedOfBalloons.length)];
      var balloon = document.getElementById(object);
      let position = [];
      (function(){
        for(let value = 0; value < 1300;){
          position.push(value)
          value += 50;
        }
      })();
      balloon.style.left = position[Math.floor(Math.random()*position.length)] + "px";
      var bottomValue = 0;
      (function move(){
        moveID = setTimeout(action, speed);
        function action() {
          if (bottomValue === 110) {
            balloon.remove();
          } else{
              balloon.style.bottom = bottomValue + "%";
              bottomValue += 0.25;
              move();
            }
        }
      })();
      onscreenBalloons = document.getElementById("balloons").childElementCount;
    })();
    (function (){
      totalBalloons = unit - onscreenBalloons;
      myscore = score +" / "+ totalBalloons;
      document.getElementsByClassName("score")[0].innerHTML = "score :" + score +" / "+ unit;
      document.getElementsByClassName("score")[1].innerHTML = "score :" + myscore;
    })();
  }
})();